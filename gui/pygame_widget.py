from abc import ABC, abstractmethod
import pygame

class PygameWidget(ABC):
    
    @abstractmethod
    def draw(self):
        pass
