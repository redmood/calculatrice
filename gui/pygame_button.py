import pygame
from gui.pygame_color import PygameColor
from gui.pygame_widget import PygameWidget
import logging
_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class PygameButton(PygameWidget):
    """
        Represents a button, for pygame
        
        Attributes :
            coords : (x, y) tuple being the up left corner’s coordinates
            height :
            width :
            thickness :
            border_color :
            color : color of the text
            font_family : font of the text
            font_size : size of the text
            background_color :

    """

    def __init__(self, coords: tuple=(0, 0), text: str='', height: int=50, width: int=150, border_thickness: int=2, color: pygame.Color=PygameColor.BLACK, font_family: str=None, font_size: int=20, background_color: pygame.Color=PygameColor.WHITE, on_focus_background_color: pygame.Color=None, border_color: pygame.Color=PygameColor.RED1):
        self.coords = coords
        self.height = height
        self.width = width
        self.border_thickness = border_thickness
        self.text_color = color
        self.font = pygame.font.Font(None, font_size) if not font_family else pygame.font.SysFont(font_family, font_size)
        self.background_color = background_color
        self.on_focus_background_color = on_focus_background_color
        self.border_color = border_color
        self.text_surface = self.font.render(text, True, self.text_color)
        x_text = self.coords[0] + (self.width - self.text_surface.get_width()) // 2 
        y_text = self.coords[1] + (self.height - self.text_surface.get_height()) // 2
        self.text_coords = (x_text, y_text)

    def set_text(self, text):
        pass

    def set_font_family(self, fontname):
        pass

    def set_font_size(self, font_size):
        pass

    def set_font(self, font_family, font_size):
        pass
    
    def set_text_color(self, color):
        pass

    def set_x(self, x):
        pass

    def set_y(self, y):
        pass

    def set_position(self, coordinates):
        pass

    def set_dimensions(self, width, height):
        pass

    def set_background_color(self, color):
        pass
    
    def choose_current_background_color(self, mouse_pos):
        _logger.debug("mouse_pos : {}".format(mouse_pos))
        if mouse_pos[0] > self.coords[0] + self.width:
            return self.background_color
        elif mouse_pos[0] < self.coords[0]:
            return self.background_color
        elif mouse_pos[1] > self.coords[1] + self.height:
            return self.background_color
        elif mouse_pos[1] < self.coords[1]:
            return self.background_color
       
        return [255 - c for c in self.background_color] if not self.on_focus_background_color else self.on_focus_background_color

    def draw(self, surface):    
        """
            Draw the button on the surface.

            WARNING: it does not trigger the screen refresh. You have to use : pygame.display.update() after it, in order to actually see the button.
        """
        #drawing button background        
        background_color = self.choose_current_background_color(pygame.mouse.get_pos())
        pygame.draw.rect(surface, background_color, [self.coords[0], self.coords[1], self.width, self.height])
        
        if self.border_thickness > 0:
            #drawing button border
            pygame.draw.rect(surface, self.border_color, [self.coords[0], self.coords[1], self.width, self.height], self.border_thickness)
        surface.blit(self.text_surface, self.text_coords)
