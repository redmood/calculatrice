import pygame
import logging
_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

class PygameView:

    def __init__(self, widgets=None):
        self.widgets = widgets if widgets else dict()

    def add_widget(self, widget, widget_name):
        self.widgets[widget_name] = widget

    def draw(self, screen):
        _logger.debug("widgets : {}".format(self.widgets))
        for widget_name in self.widgets:
            widget = self.widgets[widget_name]
            widget.draw(screen)
