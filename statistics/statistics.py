#! /usr/bin/env python3
# coding: utf-8

def basics_stats(start_time,end_time):
    spent = end_time - start_time
    spent = round(spent*100)/100
    return spent

def compute_speed(time,calc_creator):
    return (calc_creator.params["repetitions"]/time)


def compute_hit_rate(right,calc_creator):
    return (100 * (right/calc_creator.params["repetitions"]))

def compute_corrected_speed(time,nb_right):
    return nb_right/time
