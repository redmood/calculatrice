from dataclasses import dataclass

@dataclass
class WindowSettings: 
    window_width: int
    window_height: int
    window_title: str='Pygame'
