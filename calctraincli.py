#-*- coding: utf-8 -*-
"""
    CLI mental calculus
"""

import time

from calculus_creator import calculus_creator as cc
from statistics import statistics as stats
import graphics
import click

def value_of_op(op):
    op_dict = {
        "add":"+",
        "mul":"x",
        "sub":"-",
        "div":"/",
    }
    return op_dict[op]

def run_normal_mode(calc_creator, repetitions, float_result): 
    start_time = time.time()
    nb_right = 0
    for calc in calc_creator.build_calculus():
        print("{} {} {} = ?".format(
            calc["n1"],
            value_of_op(calc["operator"]),
            calc["n2"],
        ))

        answer = input()

        if float_result:
            answer = float(answer)
        else:
            answer = int(answer)

        right = calc_creator.check_res(calc, answer)
        if right:
            nb_right = nb_right + 1

        print("{}    {}/{}".format(
            ("ok" if right else "wrong: {}".format(calc['result'])),
            nb_right,
            repetitions,
            ))
    end_time = time.time()
    spent = stats.basics_stats(start_time, end_time)

    print("it took you {} seconds to solve {} calculus.".format(spent,
                                                                repetitions))
    print("you have {}% of right answers.".format(stats.compute_hit_rate(nb_right, calc_creator)))
    print("your computation speed is {} calc/min".format(stats.compute_speed(spent, calc_creator)*60))
    print("your corrected computation speed is {} calc/min (corrected speed takes only right answers into account)".format(stats.compute_corrected_speed(spent,nb_right)*60))      

def run_time_attack_mode():
    pass

@click.command()
@click.option('--repetitions', default=10, help='number of calculus proposed in a row')
@click.option('--mini', default=1, help='set min number terms in calculus')
@click.option('--maxi', default=10, help='set max number terms in calculus')
@click.option('--float-result', is_flag=True, help='calculus can have float result (precision is 3 digits after comma)')
@click.option('--negative', is_flag=True, help='results can be negative')
@click.option('--table', help='values can be: add, sub, mul or div, followed by min:size:max, ex: --table add:2:10:7, will make you work on the addition tables from 2 to 7 of size 10')
@click.option('-g', is_flag=True, help='runs in graphic mode')
@click.option('--time-attack', help='try to make as many computations as you can in the given amount of time (in seconds): --time-attack 20 -> you have 20 seconds to answer as many calculus as you can.')
def main(repetitions, mini, maxi, float_result, negative, table, g, time_attack):
    """
        Runs the main program.

        It takes several options:
            --repetitions n, will propose n calculus to solve, default being
            10: 
                ''' python calctraincli.py --repetitions 20 '''
            Will generate 20 calculus

            --mini n, --maxi M, set the minimum and maximum values for the calculus
            terms.

            --float-result, flag argument, allowing calculus with float,
            precision is 3 digits after comma.

            --negative, the calculus result can be negative, it is always
            positive by default

            --table tab:min:size:max, ex:
                --table mul:1:10, will propose you calculus extracted from
                classic multiplications tables, from 1 to 10.
                --table mul:1:20, will propose you calculus extracted from
                multiplications from 1 to 20, meaning for example: 13x1, 13x2, 13x3… can
                occur.

            --time-attack s, ex: --time-attack 20, you must answer as many
            calculus as you can, within 20 seconds.
    """

    calc_type = 'classic'

    if table:
        params = table.split(":")
        calc_type = params[0]
        params.pop(0)
        mod = len(params)
        tab = list()

        for i in range(0, 3):
            tab.append(params[i%mod])

        mini = int(tab[0])
        size = int(tab[1])
        maxi = int(tab[2])

    if calc_type == 'classic':
        calc_creator = cc.CalculusCreator({
            "repetitions":repetitions,
            "min":mini,
            "max":maxi,
            "negativeresults":negative,
            "integerresults":not float_result,
            "table": calc_type,
            "time_attack": time_attack,
        })
    else:
        calc_creator = cc.TableCalculusCreator({
            "repetitions":repetitions,
            "min":mini,
            "max":maxi,
            "negativeresults":negative,
            "integerresults":not float_result,
            "table": calc_type,
            "time_attack": time_attack,
        })
    
    if time_attack:
        print(time_attack)
    else:
        run_normal_mode(calc_creator, repetitions, float_result)

if __name__ == "__main__":
    main()
