#! /usr/bin/env python3
# coding: utf-8

import unittest
import statistics
from calculus_creator import calculus_creator

class TestStatistics(unittest.TestCase):

    def test_basic_stats(self):
        start_time = 0
        end_time = 412.4577
        spent = statistics.basics_stats(start_time,end_time)

        self.assertTrue(abs(spent - 412.46) < 0.001)

        end_time = 412.4547
        spent = statistics.basics_stats(start_time,end_time)

        self.assertTrue(abs(spent - 412.45) < 0.001)

    def test_compute_speed(self):
        calc_creator = calculus_creator.CalculusCreator({
            "repetitions":10,
            })
        time = 250
        speed = statistics.compute_speed(time,calc_creator)

        self.assertTrue(abs(speed - 25) < 0.001)

    #def test_compute_hit_rate(self):

    #def test_compute_corrected_speed(self):

if __name__ == "__main__":
    unittest.main()
