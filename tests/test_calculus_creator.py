#! /usr/bin/env python3
# coding: utf-8

import unittest
import mock
import secrets
import calculus_creator

class TestCalculusCreator(unittest.TestCase):

    @mock.patch('calculus_creator.CalculusCreator.configure')
    def test_init(self,configure_mock):
        assert configure_mock is calculus_creator.CalculusCreator.configure

        calc_creator = calculus_creator.CalculusCreator()
        configure_mock.assert_called_once_with({
                        "repetitions":1,
                        "min":1,
                        "max":10,
                        "negativeresults":False,
                        "integerresults":True,
        })

        self.assertEqual(calc_creator.accepted_parameters,[
                "repetitions",
                "min",
                "max",
                "negativeresults",
                "integerresults"])
        self.assertEqual(calc_creator.op_dict, {
            "add":calc_creator.add,
            "sub":calc_creator.sub,
            "mul":calc_creator.mul,
            "div":calc_creator.div,
        })
        self.assertEqual(calc_creator.set_dispatch, {
            "repetitions":calc_creator.set_repetitions,
            "min":calc_creator.set_min,
            "max":calc_creator.set_max,
            "negativeresults":calc_creator.set_negative_results,
            "integerresults":calc_creator.set_integer_results,
        })

    def test_randnum(self):

        calc_creator = calculus_creator.CalculusCreator()

        for i in range(0,1000):
            num = calc_creator.randnum()
            self.assertIsInstance(num,int)
            self.assertGreaterEqual(num,1)
            self.assertLessEqual(num,10)

    def test_rand_op(self):
        calc_creator = calculus_creator.CalculusCreator()

        for i in range(0,1000):
            op = calc_creator.rand_op()
            self.assertIsInstance(op,str)
            self.assertIn(op,["add","mul","div","sub"])

    def test_set_repetitions(self):
        calc_creator = calculus_creator.CalculusCreator()
        self.assertEqual(calc_creator.params["repetitions"],1)
        calc_creator.set_repetitions(25)
        self.assertEqual(calc_creator.params["repetitions"],25)

    @mock.patch('calculus_creator.CalculusCreator.set_repetitions')
    def test_configure(self,set_repetitions_mock):
        calc_creator = calculus_creator.CalculusCreator()
        with self.assertRaises(KeyError):
            calc_creator.configure({"notakey":1})

        assert set_repetitions_mock is calculus_creator.CalculusCreator.set_repetitions
        calc_creator.configure({"repetitions":5})
        assert set_repetitions_mock.called

    def test_set_min(self):
        calc_creator = calculus_creator.CalculusCreator()
        self.assertEqual(calc_creator.params["min"],1)
        self.assertEqual(calc_creator.params["max"],10)

        #reseting to a value less than max = 10
        calc_creator.set_min(5)
        self.assertEqual(calc_creator.params["min"],5)

        #attempting to set min, to a value less than 0
        calc_creator.set_min(0)
        self.assertEqual(calc_creator.params["min"],5)

        #setting min to a value higher than max = 10
        calc_creator.set_min(15)
        self.assertEqual(calc_creator.params["min"],15)
        self.assertEqual(calc_creator.params["max"],15)

    def test_set_max(self):
        calc_creator = calculus_creator.CalculusCreator()
        calc_creator.set_min(3)
        self.assertEqual(calc_creator.params["min"],3)
        self.assertEqual(calc_creator.params["max"],10)

        #reseting to a value more than min = 3
        calc_creator.set_max(5)
        self.assertEqual(calc_creator.params["max"],5)

        #attempting to set max, to a value less than 0
        calc_creator.set_max(0)
        self.assertEqual(calc_creator.params["max"],5)

        #setting max to a value les than min = 3
        calc_creator.set_max(2)
        self.assertEqual(calc_creator.params["min"],2)
        self.assertEqual(calc_creator.params["max"],2)

    def test_get_divisors_list(self):
        calc_creator = calculus_creator.CalculusCreator()

        for divisor in calc_creator.get_divisors_list(50):
            self.assertIn(divisor,[2,5,10,25])

    @mock.patch('calculus_creator.CalculusCreator.randnum')
    @mock.patch('calculus_creator.CalculusCreator.get_divisors_list')
    def test_ensure_integer_n1_divisible(self,divisors_list_mock,randnum_mock):
        assert divisors_list_mock is calculus_creator.CalculusCreator.get_divisors_list
        assert randnum_mock is calculus_creator.CalculusCreator.randnum

        randnum_mock.return_value = 50
        divisors_list_mock.return_value = [2,5,10,25]

        calc_creator = calculus_creator.CalculusCreator()
        n1,n2 = calc_creator.ensure_integer()

        self.assertEqual(n1,50)
        self.assertIn(n2,[2,5,10,25])

    @mock.patch('calculus_creator.CalculusCreator.randnum')
    @mock.patch('calculus_creator.CalculusCreator.get_divisors_list')
    def test_ensure_integer_n1_prime(self,divisors_list_mock,randnum_mock):
        assert divisors_list_mock is calculus_creator.CalculusCreator.get_divisors_list
        assert randnum_mock is calculus_creator.CalculusCreator.randnum

        randnum_mock.return_value = 47
        divisors_list_mock.return_value = []

        calc_creator = calculus_creator.CalculusCreator()
        n1,n2 = calc_creator.ensure_integer()

        self.assertEqual(n1,47)
        self.assertEqual(n2,47)

    def test_ensure_positive(self):
        calc_creator = calculus_creator.CalculusCreator()

        n1,n2 = calc_creator.ensure_positive(10,20)
        self.assertEqual(n1,20)
        self.assertEqual(n2,10)

        n1,n2 = calc_creator.ensure_positive(20,10)
        self.assertEqual(n1,20)
        self.assertEqual(n2,10)

    def test_ensure_higher_than_error(self):
        calc_creator = calculus_creator.CalculusCreator()

        n1,n2 = calc_creator.ensure_higher_than_error(4,5000)
        self.assertEqual(n1,5000)
        self.assertEqual(n2,4)

        n1,n2 = calc_creator.ensure_higher_than_error(5000,4)
        self.assertEqual(n1,5000)
        self.assertEqual(n2,4)

    def test_check_integer_res(self):
        calc_creator = calculus_creator.CalculusCreator()

        self.assertTrue(calc_creator.check_integer_res({
            "result":25
        },25))
        self.assertFalse(calc_creator.check_integer_res({
            "result":25
        },26))

    def test_check_float_res(self):
        calc_creator = calculus_creator.CalculusCreator()

        self.assertTrue(calc_creator.check_float_res({
            "result":25
        },25.0008))

        self.assertFalse(calc_creator.check_float_res({
            "result":25
        },25.001))

    @mock.patch('calculus_creator.CalculusCreator.check_integer_res')
    def test_check_res_integer(self,check_integer_mock):
        assert check_integer_mock is calculus_creator.CalculusCreator.check_integer_res
        check_integer_mock.return_value = True

        calc_creator = calculus_creator.CalculusCreator({"integerresults":True})
        self.assertTrue(calc_creator.params["integerresults"])

        calc_creator.check_res({"result":25},25)
        check_integer_mock.called

    @mock.patch('calculus_creator.CalculusCreator.check_float_res')
    def test_check_res_float(self,check_float_mock):
        assert check_float_mock is calculus_creator.CalculusCreator.check_float_res
        check_float_mock.return_value = True

        calc_creator = calculus_creator.CalculusCreator({"integerresults":False})
        self.assertFalse(calc_creator.params["integerresults"])

        calc_creator.check_res({"result":25.0},25.0)
        check_float_mock.called

    @mock.patch('calculus_creator.CalculusCreator.rand_op')
    @mock.patch('calculus_creator.CalculusCreator.randnum')
    def test_build_calculus(self,randnum_call,rand_op_call):
        assert rand_op_call is calculus_creator.CalculusCreator.rand_op
        assert randnum_call is calculus_creator.CalculusCreator.randnum

        calc_creator = calculus_creator.CalculusCreator()
        rand_op_call.return_value = "mul"
        randnum_call.return_value = 4
        for calc in calc_creator.build_calculus():
            self.assertEqual(calc, {"n1":4, "n2":4, "result":16, "operator":"mul"})

if __name__ == "__main__":
    unittest.main()
