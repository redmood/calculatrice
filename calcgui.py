#-*- coding: utf-8 -*-

import pygame
from graphics.pygame_window_settings import WindowSettings
from gui.pygame_color import PygameColor
from gui.pygame_button import PygameButton
from gui.pygame_view import PygameView
from pygame.locals import QUIT

ws = WindowSettings(window_width=700, window_height=700)

pygame.init()

#initialising main screen
screen = pygame.display.set_mode((ws.window_width, ws.window_height))
screen.fill(PygameColor.WHITE)

view = PygameView()
button = PygameButton(coords=(200, 200), background_color=PygameColor.YELLOW1, on_focus_background_color=PygameColor.INDIGO, font_family='comicsansms', font_size=30, text='Test Button', border_thickness=0)
view.add_widget(view, 'dylan_button')
#view.draw(screen)
for key in view.widgets:
    widget = view.widgets[key]
    widget.draw(screen)
pygame.display.flip()

clock = pygame.time.Clock()

play = True
while play:
    for event in pygame.event.get():
        if event.type == QUIT:
            play = False
    
    #drawing window
    screen.fill(PygameColor.WHITE)
    pygame.display.flip()
    clock.tick(80)
