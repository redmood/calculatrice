#! /usr/bin/env python3
# coding: utf-8

import random
import logging
import math

logging.basicConfig(level=logging.INFO)
_logger = logging.getLogger(__name__)

class CalculusCreator:

    def __init__(self,kwargs={
                    "repetitions":1,
                    "min":1,
                    "max":10,
                    "negativeresults":False,
                    "integerresults":True,
                    "table":None,
    }):
        self.accepted_parameters = [
                "repetitions",
                "min",
                "max",
                "negativeresults",
                "integerresults",
                "table",
                "time_attack"]
        self.set_dispatch = {
            "repetitions":self.set_repetitions,
            "min":self.set_min,
            "max":self.set_max,
            "negativeresults":self.set_negative_results,
            "integerresults":self.set_integer_results,
            "table":self.set_table,
            "time_attack":self.set_time_attack,
        }
        self.op_dict = {
            "add":self.add,
            "sub":self.sub,
            "mul":self.mul,
            "div":self.div,
        }
        self.params = {}
        self.configure(kwargs)

    def configure(self,kwargs):
        for key in kwargs:
            if key not in self.accepted_parameters:
                raise KeyError("parameter {} is not supported. supported parameters are: {}".format(key,self.accepted_parameters))
            else:
                self.set_dispatch[key](kwargs[key])

    def set_repetitions(self,repetitions):
        if repetitions > 0:
            self.params["repetitions"] = repetitions
        else:
            self.params["repetitions"] = 1

    def set_min(self,mini):
        if mini > 0:
            self.params["min"] = mini
            if "max" in self.params and mini > self.params["max"]:
                self.params["max"] = mini

    def set_max(self,maxi):
        if maxi > 0:
            self.params["max"] = maxi
            if "min" in self.params and maxi < self.params["min"]:
                self.params["min"] = maxi

    def set_negative_results(self,negativeresults):
        self.params["negativeresults"] = negativeresults

    def set_integer_results(self,integerresults):
        self.params["integerresults"] = integerresults

    def set_table(self,table):
        self.params["table"] = table

    def set_time_attack(self,time_attack):
        self.params["time_attack"] = time_attack

    def add(self,n1,n2):
        return n1 + n2

    def sub(self,n1,n2):
        return n1 - n2

    def mul(self,n1,n2):
        return n1 * n2

    def div(self,n1,n2):
        if n2 == 0:
            raise ValueError("n2 is equal to zero, can’t divide")
        else:
            return n1/n2

    def randnum(self):
        return random.randrange(self.params['min'], self.params['max'], 1)

    def rand_op(self):
        return random.choice(list(self.op_dict.keys()))

    def ensure_integer(self):
        n1 = int(self.randnum())
        _logger.debug("n1: {}".format(n1))
        divisors_list = self.get_divisors_list(n1)
        _logger.debug("divisors list: {}".format(divisors_list))

        if len(divisors_list) == 0:#divisors_list empty means n1
        #has no divisors, it is a prime number
            return (n1,n1)
        else:
            #choose n2 among n1’s divisors
            n2 = int(random.choice(divisors_list))
            _logger.debug("n2: {}".format(n2))
            return (n1,n2)

    def get_divisors_list(self,n):
        d_max = int(math.ceil(math.sqrt(n)))
        divisors_list = []

        for d in range(2,d_max):
            if n % d == 0:
                #if d divides n, then there is a k, such as:
                #d x k = n, which implies: k = n / d
                #is a divisor of n, too
                divisors_list.append(d)
                divisors_list.append(int(n/d))

        return divisors_list

    def ensure_positive(self,n1,n2):
        if n1 < n2:
            return (n2,n1)
        else:
            return (n1,n2)

    def ensure_higher_than_error(self,n1,n2):
        #if the division of n1 by n2 is too small (< 0.001),
        #then n2 is too big compared to n1, we inverse the operation
        #to make sure the result is big enough
        if (abs(n1 / n2) - 0.001) < 0:
            return (n2,n1)
        else:
            return (n1,n2)

    def build_calculus(self):
        #looping on repetitions number
        for i in range(0,self.params["repetitions"]):
            #choosing operator
            op = self.rand_op()
            _logger.debug("op: {}".format(op))

            n1 = 0
            n2 = 0
            res = 0

            #if operator is division and object were set to ensure
            #only interger results
            if op == "div" and self.params["integerresults"]:
                n1, n2 = self.ensure_integer()
                res = int(self.op_dict[op](n1,n2))
            else:
                #randomly choosing n1 and n2
                n1 = self.randnum()
                _logger.debug("n1: {}".format(n1))
                n2 = self.randnum()
                _logger.debug("n2: {}".format(n2))

                if op == "div":#we have to make sure the result is not
                #too small (< 0.001)
                    n1,n2 = self.ensure_higher_than_error(n1,n2)
                elif op == "sub" and not self.params["negativeresults"]:
                    #n1 and n2 will be swapped if n1 - n2 is negative
                    n1, n2 = self.ensure_positive(n1,n2)

                res = self.op_dict[op](n1,n2)
            _logger.debug("res: {}".format(res))

            yield {"n1":n1,
                "n2":n2,
                "result":res,
                "operator":op
                }

    def check_integer_res(self,calc,res):
        if res == calc["result"]:
            return True
        else:
            return False

    def check_float_res(self,calc,res):
        if abs(res - calc["result"]) < 0.001:
            return True
        else:
            return False

    def check_res(self,calc,res):
        _logger.debug("set for integer only: {}".format(self.params["integerresults"]))
        if self.params["integerresults"]:
            _logger.debug("using check_integer_res")
            return self.check_integer_res(calc,res)
        else:
            _logger.debug("using check_float_res")
            return self.check_float_res(calc,res)

class TableCalculusCreator(CalculusCreator):

    def build_calculus(self):
        for i in range(0,self.params["repetitions"]):
            op = self.params["table"]

            if op == "div":
                n1, n2 = self.ensure_integer()
                while n2 == n1:
                    n2 = self.randnum()
                    n1, n2 = self.ensure_integer()
            else:
                n1 = self.randnum()
                n2 = self.randnum()
                
            res = int(self.op_dict[op](n1,n2))
            yield {"n1":n1,"n2":n2,"result":res,"operator":op}
